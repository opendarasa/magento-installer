
echo -e "make requirement changes for magento on php.ini"
sudo sed -i "s/memory_limit = .*/memory_limit = 768M/" /etc/php/8.1/fpm/php.ini
sudo sed -i "s/upload_max_filesize = .*/upload_max_filesize = 128M/" /etc/php/8.1/fpm/php.ini
sudo sed -i "s/zlib.output_compression = .*/zlib.output_compression = on/" /etc/php/8.1/fpm/php.ini
sudo sed -i "s/max_execution_time = .*/max_execution_time = 18000/" /etc/php/8.1/fpm/php.ini


echo -e "copy magento config file to sites-enabled"
sudo cp myapp.local.conf /etc/nginx/sites-enabled/myapp.local.conf

echo -e "create a new Database by running <CREATE DATABASE magentodb;>"
echo -e "GRANT all privileges of newly created Database to root by running <GRANT ALL PRIVILEGES on magentodb.* to 'root'@'localhost';>"
echo -e "run FLUSH PRIVILEGES;"
sudo mysql -u root -p


echo -e "install elasticsearch dependencies"

sudo apt-get install apt-transport-https ca-certificates gnupg2 -y 

echo -e "Import the GPG key"

wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -

echo "Add elasticsearch repo"
sudo sh -c 'echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" > /etc/apt/sources.list.d/elastic-7.x.list'

echo -e "install elasticsearch"
sudo apt-get update && sudo apt-get install elasticsearch -y

echo -e "enable elasticsearch"
sudo systemctl --now enable elasticsearch

echo -e "downloading magento"

composer create-project --repository-url=https://repo.magento.com/ magento/project-community-edition=2.4.4 /opt/magento2

echo "start installation of magento"

cd /opt/magento2

bin/magento setup:install \
--base-url=http://myapp.local \
--db-host=localhost \
--db-name=magentodb \
--db-user=root \
--db-password=2455Db1756 \
--admin-firstname=admin \
--admin-lastname=admin \
--admin-email=admin@admin.com \
--admin-user=admin \
--admin-password=admin123 \
--language=en_US \
--currency=USD \
--timezone=America/Chicago \
--use-rewrites=1

echo -e "changing permissions"
sudo chown -R www-data. /opt/magento2

echo -e "disabling 2 factors authentification"
sudo -u www-data bin/magento module:disable Magento_TwoFactorAuth
sudo -u www-data bin/magento module:disable Magento_TwoFactorAuth


echo -e "setup cron job"
sudo -u www-data bin/magento cron:install

echo "All done!"