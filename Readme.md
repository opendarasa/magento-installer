# Download
	- `git clone http://gitlab.com/opendarasa/magento-installer.git`
	- cd magento-installer

# Environment set up

	1. Install LAMP (web server:NGINX , If you don't already have it installed) by running the commands below
		- `source lamp-installer.sh`
		- `source mysql-configuration.sh`

	2. Install Magento and dependencies by running the command below
		- `source magento-installer.sh` 
		- `sudo nano /etc/hosts` and add 127.0.0.1 `myapp.local`
# Requirements:

	1. PHP8.1

	2. COMPOSER 2.5.1

	3. NGINX 1.18

	4. MYSQL 8.31

	5. Ubuntu 20.04 and above

